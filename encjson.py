import json, os
from cryptography.fernet import Fernet
import sys

class encjson(object):
    def __init__(self, key):
        self.key = Fernet(key.encode())

    def load(self, file):
        return json.loads(self.key.decrypt(file.read()))

    def dump(self, data, file):
        return file.write(self.key.encrypt(json.dumps(data, indent=2).encode()))

    def loads(self, data):
        return json.loads(self.key.decrypt(data))

    def dumps(self, data):
        return self.key.encrypt(json.dumps(data))
