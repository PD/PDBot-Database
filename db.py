from flask import Flask, request
from gevent.pywsgi import WSGIServer
from encjson import encjson
from in_use import in_use
import sys, os
from time import sleep

ignore = [".git", ".gitignore", ".replit", ".env"]

for file in os.listdir('.'):
    if file not in ignore and file.startswith('.'):
        os.system("rm -rf "+file)

json=encjson(sys.argv[1])
auth_key=sys.argv[2]
del(sys, ignore)

app=Flask(__name__)

@app.route('/')
def index():
    return 'HEY! You are not supposed to see this page!'

@app.route('/<path:path>', methods=['POST', 'GET'])
def file_viewer(path):
    try:
        if request.json['auth_key'] != auth_key:
            print(request.json['auth_key'])
            return 'Auth key is incorrect', 401
    except Exception as e:
        print(e)
        return 'Auth key is incorrect', 401
    if request.method == 'GET':
        while in_use(name=f".{path.strip('/')}").check(): sleep(0.5)
        try:
            in_use(name=f".{path.strip('/')}").state(True)
            with open(f"./data/{path}", "rb") as f:
                data = json.load(f)
            in_use(name=f".{path.strip('/')}").state(False)
            return data, 200
        except FileNotFoundError:
            return "File not found", 404
    if request.method == 'POST':
        try:
            while in_use(name=f".{path.strip('/')}").check(): sleep(0.3)
            with open(f"./data/{path}", "wb") as f:
                in_use(name=f".{path.strip('/')}").state(True)
                json.dump(request.json['data'], f)
            in_use(name=f".{path.strip('/')}").state(False)
            return "File written successfully", 200
        except:
            try:
                p=path.split('/')
                o=''
                for i in p:
                    o=o+i+'/'
                    if i == p[-1]:
                        break
                    os.system(f'mkdir ./data/{o}')
                    with open(f"./data/{path}", "wb") as f:
                        in_use(name=f".{path.strip('/')}").state(True)
                        json.dump(request.json['data'], f)
                    in_use(name=f".{path.strip('/')}").state(False) 
                    return "File written successfully", 200
            except Exception as e:
                print(e)
                in_use(name=f".{path.strip('/')}").state(False)
                return str(e)

WSGIServer(("0.0.0.0", 8000), app).serve_forever()
