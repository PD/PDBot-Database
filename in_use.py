from os import unlink, path

class InvalidState(Exception):
    pass

class in_use:
    def __init__(self, state=False, name=".in_use"):
        if not isinstance(state, bool):
            raise InvalidState("Invalid state "+repr(state))
            raise SystemExit
        if state:
            with open(name.strip('\\').replace('/', ''), "w+") as f:
                pass
        self.name=name.strip('\\').replace('/', '')

    def check(self):
        return (path.exists(self.name.strip('\\').strip('/')))

    def state(self, state):
        if state not in (True, False):
            raise InvalidState("Invalid State "+repr(state))
            raise SystemExit
        if state:
            with open(self.name.strip('\\').strip('/'), "w+"):
                pass
        if not state:
            try:
                unlink(self.name.strip('\\').strip('/'))
            except:
                pass
        return
